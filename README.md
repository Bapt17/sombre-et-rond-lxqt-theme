sombre-et-rond lxqt theme:
- A dark, rounded and modern lxqt theme based on dark lxqt theme https://github.com/lxqt/lxqt-themes/tree/master/themes/dark and yaru rounded theme for lxqt 
- Button from Lubuntu Arc LXQT theme
- Buttons from https://material.io/resources/icons 
- **Screenshot**:

![Screenshot](screen.png)

- **Installation**
You can download it from [opendesktop.org](https://www.opendesktop.org/p/1499769) and extract into /usr/share/lxqt/themes
